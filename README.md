# Manifest files for building ROMs

I created these files for use when building unofficial and custom [`e /os`](https://e.foundation/)  and [`LineageOS for microG`](https://lineage.microg.org/) ROMS for my Sony Xperia phones: XZ1 Compact (lilac), Z5 Compact (suzuran), and Z3 Compact (z3c). /e/ plan eventually to produce official builds for devices that have official Lineage OS 17.1 builds, and are in demand in the /e/ community forums, but it may be while before the /e/ developers have the time to produce official ROMs for all those devices. 

Z1 Compact (lilac)
-
For lilac, there is no official Lineage build, but there is an [unofficial 17.1 ROM](https://forum.xda-developers.com/t/rom-lineageos-17-1-unofficial-1-4-update-2020-08-09.4047763/), and my manifest pulls in the kernel and device trees from the repos used to build that ROM. I made several builds using the proprietary vendor files from that ROM's author's repos, but those builds would never boot, and I suspected a mismatch between the device trees and the proprietary binary files. So to get a working build, I flashed the unofficial Lineage ROM, extracted the blobs from the flashed phone and pushed them to a [new repo](https://git.coop/phone-custom-roms/vendor_sony_lilac). So the lilac manifest points to the kernel and device repos used for the unofficial ROM, and the vendor files manifest points to my new repo. To build using this manifest, you need the line `-e "INCLUDE_PROPRIETARY=false" \ ` in the Docker build command. 

Z5 Compact (suzuran)
-
For suzuran, the last official Lineage ROM was for nougat, but there is an [unofficial 17.1 ROM](https://forum.xda-developers.com/t/rom-unofficial-10-q-lineageos-17-1-for-z5c-suzuran.4052973/). I can build successfully using the kernel and device trees, and the proprietary vendor files used in that ROM, so the manifest points to those repos. To build using this manifest, you need the line `-e "INCLUDE_PROPRIETARY=false" \ ` in the Docker build command. 

Z3 Compact (z3c)
-
There is an [official Lineage 17.1 build](https://forum.xda-developers.com/t/official-lineageos-17-1-for-z3c.4160347/), but I didn't want to wait until /e/ developers have the time to produce an official ROM. Because there is an Official Lineage 17.1 build, there is no need for a manifest to pull in the device files from the [Lineage repo](https://github.com/LineageOS/android_device_sony_z3c), and the vendor files can be picked up from [The Muppets](https://github.com/TheMuppets/proprietary_vendor_sony/tree/lineage-17.1), if you set `-e "INCLUDE_PROPRIETARY=true" \` in the Docker build command. However, doing that when building lilac and / or suzuran in the same Docker build run, causes a `repo sync`  error when using the other manifests here (`fatal: duplicate path vendor/sony in /srv/src/Q/.repo/manifests/default.xml`). To get round this, and to allow me to build for all three devices in a single Docker build run, I have extracted the vendor specific files and pushed them to new repos for [z3c specific](https://git.coop/phone-custom-roms/vendor_sony_z3c) and [shinano common](https://git.coop/phone-custom-roms/vendor_sony_shinano-common) files. The vendor files manifest points to these repos.

Sony Vendor files
-
As well as the path and project information, this manifest defines a new `gitcoop` remote, pointing to [git.coop](https://git.coop). This was tested in local a build on Wed 9 December 2020 and it worked: the necessary files were checked out to the `vendor/sony/` directory in the build tree. In future, this definition may move to its own file.

`one-offs`
-
Sometimes I try to build ROMs for devices which somebody has asked for in the forums. If I ca make the build work, I will add the manifest I used in the `one-offs` directory, so that I can return to them if necessary.
